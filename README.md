# SP Symfony

This is another "budget" webapp, where you will enter your income/outcome, and the app will generate detailed reports and classy charts.

![https://gitlab.com/lpdwca-symfony/sp-symfony/uploads/355f17569a0bbbed2b755909c07f90fb/image.png](https://gitlab.com/lpdwca-symfony/sp-symfony/uploads/355f17569a0bbbed2b755909c07f90fb/image.png)

![https://gitlab.com/lpdwca-symfony/sp-symfony/uploads/efaa4be7750dce7462cea16bc2e2981d/image.png](https://gitlab.com/lpdwca-symfony/sp-symfony/uploads/efaa4be7750dce7462cea16bc2e2981d/image.png)

![https://gitlab.com/lpdwca-symfony/sp-symfony/uploads/9aa39da17e9ac371a972db31cb24b61c/image.png](https://gitlab.com/lpdwca-symfony/sp-symfony/uploads/9aa39da17e9ac371a972db31cb24b61c/image.png)

## Install

```
composer install
```

## Launch dev server

```
symfony server:start 
```

 * Make sure to have mysql running with a database (edit line 28 of `.env.local` file)!
 * Make sure to have executed all migrations (`php bin/console doctrine:migrations:migrate`)!
