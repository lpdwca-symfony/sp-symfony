$(document).ready(function() {
    $('.datepicker').datepicker({
        'format': 'yyyy-mm-dd'
    });

    function updateTransactionTypes(typeMontantElement) {
        let isincome = $(typeMontantElement).is(':checked') ? 1 : 0;

        if(isincome == 1) {
            $('#form_transaction_type option').each(function(index, element) {
                if($(element).data('isincome') == 1)
                    $(element).show();
                else
                    $(element).hide();
            });
        }
        else {
            $('#form_transaction_type option').each(function(index, element) {
                if($(element).data('isincome') == 1)
                    $(element).hide();
                else
                    $(element).show();
            });
        }

        let ok = true;

        $('#form_transaction_type option').each(function(index, element) {
            if($(this).data('isincome') != isincome)
                ok = false;
        });
        
        if(!ok) {
            $('#form_transaction_type option').each(function(index, element) {
                if($(this).data('isincome') == isincome && !ok) {
                    $('#form_transaction_type').val($(this).val());
                    ok = true;
                }

            });
        }
    }

    $('.type-montant input[type=checkbox]').change(function() {
        updateTransactionTypes(this);
    });

    updateTransactionTypes($('.type-montant input[type=checkbox]'));
});