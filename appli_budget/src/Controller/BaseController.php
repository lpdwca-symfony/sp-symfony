<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\Material\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Options\PieChart\PieSlice;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



class BaseController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        $transactionsRepository = $entityManager->getRepository(Transaction::class);

        if($user){
            $transactions = $entityManager->getRepository(Transaction::class)->findBy(
                ['user' => $user],
                ['date' => 'desc', 'id' => 'desc']
            );

            $solde = 0;

            foreach($transactions as $transaction) {
                if($transaction->getTransactionType()->getIsIncome()) 
                    $solde += $transaction->getAmount();
                else 
                    $solde -= $transaction->getAmount();
            }

            $pages = $paginator->paginate(
                $transactions,
                $request->query->getInt('page', 1),
                15
            );

            if(sizeof($transactions)){
                $chart = $this->getChart($transactions);

                $data = $this->getTransactionsOrderedByType($entityManager);
                array_unshift($data[0], ["Name", "Amount"]);
                array_unshift($data[1], ["Name", "Amount"]);
                $pieChart = $this->getPieChart($data[0]);
                $pieChart2 = $this->getPieChart($data[1]);
            }
            else{
                $chart = NULL;
                $pieChart = NULL;
            }
        }        
        else{
            $pages = NULL;
            $transactions = NULL;
            $chart = NULL;
            $pieChart = NULL;
            $pieChart2 = NULL;
            $solde = 0;
        }

        return $this->render('index.html.twig', [
            'pages' => $pages,
            'transactions' => $transactions,
            'solde' => $solde,
            'chart' => $chart,
            'pieChart' => $pieChart,
            'pieChart2' => $pieChart2
        ]);
    }

    private function getTransactionsOrderedByType($em){
        $qb = $em->createQueryBuilder('transaction');
    
        $qb->select('SUM(transaction.amount) AS totalAmount, tt.name, tt.isIncome')
            ->from('App\Entity\Transaction', 'transaction')
            ->innerJoin('transaction.transactionType', 'tt', 'WITH', 'transaction.transactionType = transaction.transactionType')
            ->andWhere('transaction.user = :user')
                ->setParameter('user', $this->getUser()->getId())
            ->addSelect('identity(transaction.transactionType)')
            ->groupBy('transaction.transactionType')
            ->orderBy('totalAmount', 'DESC');
    
        $transactions = $qb->getQuery()->getArrayResult();

        $income = [];
        $outcome = [];

        for($i = 0; $i<sizeof($transactions); $i++){
            unset($transactions[$i][1]); // remove id of transaction type
            if($transactions[$i]['isIncome']){
                unset($transactions[$i]['isIncome']); // remove bool
                array_push($income, $transactions[$i]);
            }
            else{
                unset($transactions[$i]['isIncome']); // remove bool
                array_push($outcome, $transactions[$i]);
            }
        }

        // transorm associative array into simple array
        $income_values = [];
        for($i = 0; $i<sizeof($income); $i++)
            array_push($income_values, [$income[$i]["name"], intval($income[$i]["totalAmount"])]);
        $outcome_values = [];
        for($i = 0; $i<sizeof($outcome); $i++)
            array_push($outcome_values, [$outcome[$i]["name"], intval($outcome[$i]["totalAmount"])]);

        return [$income_values, $outcome_values]; 
    }

    private function getChart($transactions){
        $transactions = array_reverse($transactions);
        $curDate = [date_format($transactions[0]->getDate(), "m"), date_format($transactions[0]->getDate(), "Y")];
        $transactions_values = [['Month', 'Income', 'Outcome', 'Sum']];
        $income = 0;
        $outcome = 0;
        $total = 0;

        $max = sizeof($transactions);
        for($i = 0; $i<sizeof($transactions); $i++){
            if($curDate[0] != date_format($transactions[$i]->getDate(), "m") || $curDate[1] !=  date_format($transactions[$i]->getDate(), "Y")){
                array_push($transactions_values, ["$curDate[0]/$curDate[1]", $income, $outcome, $total]);
                $income = 0;
                $outcome = 0;
                $total = 0;
            }
            if($transactions[$i]->getTransactionType()->getIsIncome()){ // add income to current month
                $income += $transactions[$i]->getAmount();
                $total += $transactions[$i]->getAmount();
            }
            else{ // add outcome to current month
                $outcome -= $transactions[$i]->getAmount();
                $total -= $transactions[$i]->getAmount();
            }
            if($i == sizeof($transactions) - 1){ // if last value then save month
                array_push($transactions_values, ["$curDate[0]/$curDate[1]", $income, $outcome, $total]);
            }
                $curDate = [date_format($transactions[$i]->getDate(), "m"), date_format($transactions[$i]->getDate(), "Y")];
        }

        $chart = new ColumnChart();
        $chart->getData()->setArrayToDataTable($transactions_values);
        
        $chart->getOptions()->getChart()
            ->setTitle('Summary')
            ->setSubtitle('Sum of income, outcome, and (income - outcome) by month');
        $chart->getOptions()
            ->setBars('vertical')
            ->setHeight(400)
            ->setWidth('100%')
            ->setColors(['#1b9e77', '#d95f02', '#7570b3'])
            ->getVAxis()
                ->setFormat('decimal');
    
        return $chart;
    }

    private function getPieChart($data){
        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            array_values($data)
        );
        $pieChart->getOptions()->setHeight(250);
        $pieChart->getOptions()->setWidth("45%");

        return $pieChart;
    }


    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $user = new User();

        $form = $this->createFormBuilder($user)
                    ->add('username', TextType::class)
                    ->add('password', PasswordType::class)
                    ->add('save', SubmitType::class, ['label' => 'Register'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $user->setPassword($passwordEncoder->encodePassword(
                $user,
                $user->getPassword()
            ));

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     *  @Route("/settings", name="manage_account")
    */
    public function user(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entityManager = $this->getDoctrine()->getManager();

        $user = $entityManager->getRepository(User::class)->find($this->getUser()->getId());

        if(!$user)
            return $this->redirectToRoute('index');

        $form = $this->createFormBuilder($user)
                    ->add('username', TextType::class)
                    ->add('password', PasswordType::class)
                    ->add('save', SubmitType::class, ['label' => 'Edit profile'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $user->setPassword($passwordEncoder->encodePassword(
                $user,
                $user->getPassword()
            ));

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('user_settings.twig', [
            'form' => $form->createView(),
            'title' => 'Manage user settings'
        ]);
    }

}
