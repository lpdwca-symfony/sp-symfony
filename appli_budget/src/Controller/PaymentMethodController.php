<?php

namespace App\Controller;

use App\Entity\PaymentMethod;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


/**
 * @Route("/payment_method")
 */
class PaymentMethodController extends AbstractController
{
    /**
     * @Route("", name="payment_method")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $paymentMethodsRepository = $entityManager->getRepository(PaymentMethod::class);

        $paymentMethods = $paymentMethodsRepository->findAll();

        return $this->render('payment_method/index.html.twig', [
            'payment_methods' => $paymentMethods
        ]);
    }

    /**
     * @Route("/add", name="payment_method_add")
     */
    public function add(Request $request) {
        $paymentMethod = new PaymentMethod();

        $form = $this->createFormBuilder($paymentMethod)
                    ->add('name', TextType::class)
                    ->add('save', SubmitType::class, ['label' => 'Add'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $paymentMethod = $form->getData();


            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($paymentMethod);
            $entityManager->flush();

            return $this->redirectToRoute('payment_method');
        }

        return $this->render('payment_method/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Add a payment method'
        ]);
    }

    /**
     * @Route("/edit/{id}", name="payment_method_edit")
     */
    public function edit(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $paymentMethod = $entityManager->getRepository(PaymentMethod::class)->find($id);

        if(!$paymentMethod)
            return $this->redirectToRoute('payment_method');

        $form = $this->createFormBuilder($paymentMethod)
                    ->add('name', TextType::class)
                    ->add('save', SubmitType::class, ['label' => 'Edit'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $paymentMethod = $form->getData();

            $entityManager->persist($paymentMethod);
            $entityManager->flush();

            return $this->redirectToRoute('payment_method');
        }

        return $this->render('payment_method/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Edit the "'.$paymentMethod->getName().'" payment method'
        ]);
    }


    /**
     * @Route("/delete/{id}", name="payment_method_delete")
     */
    public function delete(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $paymentMethod = $entityManager->getRepository(PaymentMethod::class)->find($id);

        if($paymentMethod !== null) {
            $entityManager->remove($paymentMethod);
            $entityManager->flush();
        }

        return $this->redirectToRoute('payment_method');
    }
}
