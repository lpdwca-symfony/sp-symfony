<?php

namespace App\Controller;

use App\Entity\PaymentMethod;
use App\Entity\Transaction;
use App\Entity\TransactionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/transaction")
 */
class TransactionController extends AbstractController
{
    /**
     * @Route("", name="transaction")
     */
    public function index()
    {
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/add", name="transaction_add")
     */
    public function add(Request $request) {

        $transaction = new Transaction();

        $entityManager = $this->getDoctrine()->getManager();

        $transactionTypes = $entityManager->getRepository(TransactionType::class)->findAll();
        $paymentMethods = $entityManager->getRepository(PaymentMethod::class)->findAll();

        $isIncome = [];

        foreach($transactionTypes as $transactionType) {
            if($transactionType->getIsIncome())
                $isIncome[] = ['data-isincome' => 1];
            else
                $isIncome[] = ['data-isincome' => 0];
        }


        $form = $this->createFormBuilder($transaction)
                    ->add('libelle', TextType::class)
                    ->add('amount', NumberType::class)
                    ->add('date', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
                    ->add('transaction_type', ChoiceType::class, 
                            [
                                'choices' => $transactionTypes, 
                                'choice_value' => 'id', 
                                'choice_label' => 'name', 
                                'choice_attr' => $isIncome
                            ]
                        )
                    ->add('payment_method', ChoiceType::class, ['choices' => $paymentMethods, 'choice_value' => 'id', 'choice_label' => 'name'])
                    ->add('save', SubmitType::class, ['label' => 'Add'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $transaction = $form->getData();
            $transaction->setUser($this->getUser());

            $entityManager->persist($transaction);
            $entityManager->flush();

            return $this->redirectToRoute('transaction');
        }

        return $this->render('transaction/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Add a transaction'
        ]);
    }

    /**
     * @Route("/edit/{id}", name="transaction_edit")
     */
    public function edit(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $transaction = $entityManager->getRepository(Transaction::class)->find($id);

        if(!$transaction)
            return $this->redirectToRoute('transaction');

        $transactionTypes = $entityManager->getRepository(TransactionType::class)->findAll();
        $paymentMethods = $entityManager->getRepository(PaymentMethod::class)->findAll();

        $isIncome = [];

        foreach($transactionTypes as $transactionType) {
            if($transactionType->getIsIncome())
                $isIncome[] = ['data-isincome' => 1];
            else
                $isIncome[] = ['data-isincome' => 0];
        }


        $form = $this->createFormBuilder($transaction)
                    ->add('libelle', TextType::class)
                    ->add('amount', NumberType::class)
                    ->add('date', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
                    ->add('transaction_type', ChoiceType::class, 
                            [
                                'choices' => $transactionTypes, 
                                'choice_value' => 'id', 
                                'choice_label' => 'name', 
                                'choice_attr' => $isIncome
                            ]
                        )
                    ->add('payment_method', ChoiceType::class, ['choices' => $paymentMethods, 'choice_value' => 'id', 'choice_label' => 'name'])
                    ->add('save', SubmitType::class, ['label' => 'Add'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $transaction = $form->getData();
            $transaction->setUser($this->getUser());

            $entityManager->persist($transaction);
            $entityManager->flush();

            return $this->redirectToRoute('transaction');
        }

        return $this->render('transaction/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Edit transaction'
        ]);
    }


    /**
     * @Route("/delete/{id}", name="transaction_delete")
     */
    public function delete(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $transaction = $entityManager->getRepository(Transaction::class)->find($id);

        if($transaction !== null) {
            $entityManager->remove($transaction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('transaction');
    }
}
