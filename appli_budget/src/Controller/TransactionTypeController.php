<?php

namespace App\Controller;

use App\Entity\TransactionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/transaction_type")
 */
class TransactionTypeController extends AbstractController
{
    /**
     * @Route("", name="transaction_type")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $transactionTypesRepository = $entityManager->getRepository(TransactionType::class);

        $transactionTypes = $transactionTypesRepository->findAll();

        return $this->render('transaction_type/index.html.twig', [
            'transaction_types' => $transactionTypes
        ]);
    }

    /**
     * @Route("/add", name="transaction_type_add")
     */
    public function add(Request $request) {
        $transactionType = new TransactionType();

        $form = $this->createFormBuilder($transactionType)
                    ->add('name', TextType::class)
                    ->add('is_income', CheckboxType::class, ['required' => false])
                    ->add('save', SubmitType::class, ['label' => 'Add'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $transactionType = $form->getData();


            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($transactionType);
            $entityManager->flush();

            return $this->redirectToRoute('transaction_type');
        }

        return $this->render('transaction_type/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Add a transaction type'
        ]);
    }

    /**
     * @Route("/edit/{id}", name="transaction_type_edit")
     */
    public function edit(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $transactionType = $entityManager->getRepository(TransactionType::class)->find($id);

        if(!$transactionType)
            return $this->redirectToRoute('transaction_type');

        $form = $this->createFormBuilder($transactionType)
                    ->add('name', TextType::class)
                    ->add('is_income', CheckboxType::class, ['required' => false])
                    ->add('save', SubmitType::class, ['label' => 'Edit'])
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $transactionType = $form->getData();

            $entityManager->persist($transactionType);
            $entityManager->flush();

            return $this->redirectToRoute('transaction_type');
        }

        return $this->render('transaction_type/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Edit the "'.$transactionType->getName().'" transaction type'
        ]);
    }


    /**
     * @Route("/delete/{id}", name="transaction_type_delete")
     */
    public function delete(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $transactionType = $entityManager->getRepository(TransactionType::class)->find($id);

        if($transactionType !== null) {
            $entityManager->remove($transactionType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('transaction_type');
    }
}
