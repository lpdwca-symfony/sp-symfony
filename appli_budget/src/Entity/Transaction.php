<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $libelle;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TransactionType", inversedBy="transactions")
     */
    private $transactionType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PaymentMethod", inversedBy="transactions")
     */
    private $paymentMethod;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="transactions")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the value of libelle
     */ 
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self
     */ 
    public function setLibelle($libelle): Transaction
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get the value of amount
     */ 
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * Set the value of amount
     *
     * @return  self
     */ 
    public function setAmount($amount): Transaction
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date): Transaction
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of transactionType
     */ 
    public function getTransactionType(): ?TransactionType
    {
        return $this->transactionType;
    }

    /**
     * Set the value of transactionType
     *
     * @return  self
     */ 
    public function setTransactionType($transactionType): Transaction
    {
        $this->transactionType = $transactionType;

        return $this;
    }



    /**
     * Get the value of paymentMethod
     */ 
    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * Set the value of paymentMethod
     *
     * @return  self
     */ 
    public function setPaymentMethod($paymentMethod): Transaction
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get the value of user
     */ 
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user): Transaction
    {
        $this->user = $user;

        return $this;
    }
}
