<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransactionTypeRepository")
 */
class TransactionType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isIncome;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="transactionType")
     */
    private $transactions;

    public function __construct() {
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the value of name
     */ 
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name): TransactionType
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Get the value of isIncome
     */ 
    public function getIsIncome(): ?bool
    {
        return $this->isIncome;
    }

    /**
     * Set the value of isIncome
     *
     * @return  self
     */ 
    public function setIsIncome($isIncome): TransactionType
    {
        $this->isIncome = $isIncome;

        return $this;
    }

    /**
     * Get the value of transactions
     */ 
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }
}
